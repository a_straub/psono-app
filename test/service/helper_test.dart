import 'package:flutter_test/flutter_test.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/model/parsed_url.dart';

void main() {
  group('helper', () {
    test('parse_url www domain', () {
      expect(
          helper.parseUrl('https://www.example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'https',
              authority: 'example.com',
              fullDomain: 'example.com',
              topDomain: 'example.com',
              port: 443,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url top lvl domain', () {
      expect(
          helper.parseUrl('https://example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'https',
              authority: 'example.com',
              fullDomain: 'example.com',
              topDomain: 'example.com',
              port: 443,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url sub domain', () {
      expect(
          helper.parseUrl('http://test.example.com/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: 'test.example.com',
              fullDomain: 'test.example.com',
              topDomain: 'example.com',
              port: 80,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url sub domain with port', () {
      expect(
          helper.parseUrl('http://test.example.com:6000/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: 'test.example.com:6000',
              fullDomain: 'test.example.com',
              topDomain: 'example.com',
              port: 6000,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url ip', () {
      expect(
          helper.parseUrl('http://192.168.4.3/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: '192.168.4.3',
              fullDomain: '192.168.4.3',
              topDomain: '192.168.4.3',
              port: 80,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });

    test('parse_url ip with port', () {
      expect(
          helper.parseUrl('http://192.168.4.3:5000/url-part/#is-not-part'),
          ParsedUrl(
              scheme: 'http',
              authority: '192.168.4.3:5000',
              fullDomain: '192.168.4.3',
              topDomain: '192.168.4.3',
              port: 5000,
              path: '/url-part/',
              query: '',
              fragment: 'is-not-part'));
    });
  });
}
