import 'package:flutter/material.dart';
import './folder.dart' as componentFolder;
import './item.dart' as componentItem;
import 'package:psono/model/datastore.dart' as datastoreModel;

class FolderSelectTree extends StatelessWidget {
  final datastoreModel.Folder root;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;
  final List<String> selectedFolders;
  final List<String> selectedItems;
  final datastoreModel.FolderCallback onSelectFolder;
  final datastoreModel.ItemCallback onSelectItem;

  FolderSelectTree(
      {this.root,
      this.datastore,
      this.share,
      this.path,
      this.relativePath,
      this.selectedFolders,
      this.selectedItems,
      this.onSelectFolder,
      this.onSelectItem});

  @override
  Widget build(BuildContext context) {
    int _getFolderCount() {
      int folderCount = 0;
      if (root == null) {
        return 0;
      }
      if (root.folders != null) {
        folderCount = root.folders.length;
      }
      return folderCount;
    }

    int _getItemCount() {
      int itemCount = 0;
      if (root == null) {
        return 0;
      }
      if (root.items != null) {
        itemCount = root.items.length;
      }
      return itemCount;
    }

    int _calculateEntryCount() {
      return _getFolderCount() + _getItemCount();
    }

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        childAspectRatio: 1.0,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          int folderCount = _getFolderCount();
          if (index < folderCount) {
            datastoreModel.Folder folder = root.folders[index];
            return componentFolder.Folder(
              folder: folder,
              onPressed: () async {
                if (onSelectFolder != null) {
                  onSelectFolder(folder);
                }
              },
              isShare: folder.shareId != null,
              color: Color(0xFF151f2b),
              showSelectBox: true,
              isSelected: selectedFolders.contains(folder.id),
            );
          } else {
            datastoreModel.Item item = root.items[index - folderCount];
            return componentItem.Item(
              item: item,
              onPressed: () async {
                if (onSelectItem != null) {
                  onSelectItem(item);
                }
              },
              isShare: item.shareId != null,
              color: Color(0xFF151f2b),
              showSelectBox: true,
              isSelected: selectedItems.contains(item.id),
            );
          }
        },
        childCount: _calculateEntryCount(),
      ),
    );
  }
}
