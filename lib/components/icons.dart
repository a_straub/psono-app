import 'package:flutter/widgets.dart';

class FontAwesome {
  static const IconData glass = IconData(61440, fontFamily: "fontawesome");
  static const IconData music = IconData(61441, fontFamily: "fontawesome");
  static const IconData search = IconData(61442, fontFamily: "fontawesome");
  static const IconData envelope_o = IconData(61443, fontFamily: "fontawesome");
  static const IconData heart = IconData(61444, fontFamily: "fontawesome");
  static const IconData star = IconData(61445, fontFamily: "fontawesome");
  static const IconData star_o = IconData(61446, fontFamily: "fontawesome");
  static const IconData user = IconData(61447, fontFamily: "fontawesome");
  static const IconData film = IconData(61448, fontFamily: "fontawesome");
  static const IconData th_large = IconData(61449, fontFamily: "fontawesome");
  static const IconData th = IconData(61450, fontFamily: "fontawesome");
  static const IconData th_list = IconData(61451, fontFamily: "fontawesome");
  static const IconData check = IconData(61452, fontFamily: "fontawesome");
  static const IconData remove = IconData(61453, fontFamily: "fontawesome");
  static const IconData close = IconData(61453, fontFamily: "fontawesome");
  static const IconData times = IconData(61453, fontFamily: "fontawesome");
  static const IconData search_plus =
      IconData(61454, fontFamily: "fontawesome");
  static const IconData search_minus =
      IconData(61456, fontFamily: "fontawesome");
  static const IconData power_off = IconData(61457, fontFamily: "fontawesome");
  static const IconData signal = IconData(61458, fontFamily: "fontawesome");
  static const IconData gear = IconData(61459, fontFamily: "fontawesome");
  static const IconData cog = IconData(61459, fontFamily: "fontawesome");
  static const IconData trash_o = IconData(61460, fontFamily: "fontawesome");
  static const IconData home = IconData(61461, fontFamily: "fontawesome");
  static const IconData file_o = IconData(61462, fontFamily: "fontawesome");
  static const IconData clock_o = IconData(61463, fontFamily: "fontawesome");
  static const IconData road = IconData(61464, fontFamily: "fontawesome");
  static const IconData download = IconData(61465, fontFamily: "fontawesome");
  static const IconData arrow_circle_o_down =
      IconData(61466, fontFamily: "fontawesome");
  static const IconData arrow_circle_o_up =
      IconData(61467, fontFamily: "fontawesome");
  static const IconData inbox = IconData(61468, fontFamily: "fontawesome");
  static const IconData play_circle_o =
      IconData(61469, fontFamily: "fontawesome");
  static const IconData rotate_right =
      IconData(61470, fontFamily: "fontawesome");
  static const IconData repeat = IconData(61470, fontFamily: "fontawesome");
  static const IconData refresh = IconData(61473, fontFamily: "fontawesome");
  static const IconData list_alt = IconData(61474, fontFamily: "fontawesome");
  static const IconData lock = IconData(61475, fontFamily: "fontawesome");
  static const IconData flag = IconData(61476, fontFamily: "fontawesome");
  static const IconData headphones = IconData(61477, fontFamily: "fontawesome");
  static const IconData volume_off = IconData(61478, fontFamily: "fontawesome");
  static const IconData volume_down =
      IconData(61479, fontFamily: "fontawesome");
  static const IconData volume_up = IconData(61480, fontFamily: "fontawesome");
  static const IconData qrcode = IconData(61481, fontFamily: "fontawesome");
  static const IconData barcode = IconData(61482, fontFamily: "fontawesome");
  static const IconData tag = IconData(61483, fontFamily: "fontawesome");
  static const IconData tags = IconData(61484, fontFamily: "fontawesome");
  static const IconData book = IconData(61485, fontFamily: "fontawesome");
  static const IconData bookmark = IconData(61486, fontFamily: "fontawesome");
  static const IconData print = IconData(61487, fontFamily: "fontawesome");
  static const IconData camera = IconData(61488, fontFamily: "fontawesome");
  static const IconData font = IconData(61489, fontFamily: "fontawesome");
  static const IconData bold = IconData(61490, fontFamily: "fontawesome");
  static const IconData italic = IconData(61491, fontFamily: "fontawesome");
  static const IconData text_height =
      IconData(61492, fontFamily: "fontawesome");
  static const IconData text_width = IconData(61493, fontFamily: "fontawesome");
  static const IconData align_left = IconData(61494, fontFamily: "fontawesome");
  static const IconData align_center =
      IconData(61495, fontFamily: "fontawesome");
  static const IconData align_right =
      IconData(61496, fontFamily: "fontawesome");
  static const IconData align_justify =
      IconData(61497, fontFamily: "fontawesome");
  static const IconData list = IconData(61498, fontFamily: "fontawesome");
  static const IconData dedent = IconData(61499, fontFamily: "fontawesome");
  static const IconData outdent = IconData(61499, fontFamily: "fontawesome");
  static const IconData indent = IconData(61500, fontFamily: "fontawesome");
  static const IconData video_camera =
      IconData(61501, fontFamily: "fontawesome");
  static const IconData photo = IconData(61502, fontFamily: "fontawesome");
  static const IconData image = IconData(61502, fontFamily: "fontawesome");
  static const IconData picture_o = IconData(61502, fontFamily: "fontawesome");
  static const IconData pencil = IconData(61504, fontFamily: "fontawesome");
  static const IconData map_marker = IconData(61505, fontFamily: "fontawesome");
  static const IconData adjust = IconData(61506, fontFamily: "fontawesome");
  static const IconData tint = IconData(61507, fontFamily: "fontawesome");
  static const IconData edit = IconData(61508, fontFamily: "fontawesome");
  static const IconData pencil_square_o =
      IconData(61508, fontFamily: "fontawesome");
  static const IconData share_square_o =
      IconData(61509, fontFamily: "fontawesome");
  static const IconData check_square_o =
      IconData(61510, fontFamily: "fontawesome");
  static const IconData arrows = IconData(61511, fontFamily: "fontawesome");
  static const IconData step_backward =
      IconData(61512, fontFamily: "fontawesome");
  static const IconData fast_backward =
      IconData(61513, fontFamily: "fontawesome");
  static const IconData backward = IconData(61514, fontFamily: "fontawesome");
  static const IconData play = IconData(61515, fontFamily: "fontawesome");
  static const IconData pause = IconData(61516, fontFamily: "fontawesome");
  static const IconData stop = IconData(61517, fontFamily: "fontawesome");
  static const IconData forward = IconData(61518, fontFamily: "fontawesome");
  static const IconData fast_forward =
      IconData(61520, fontFamily: "fontawesome");
  static const IconData step_forward =
      IconData(61521, fontFamily: "fontawesome");
  static const IconData eject = IconData(61522, fontFamily: "fontawesome");
  static const IconData chevron_left =
      IconData(61523, fontFamily: "fontawesome");
  static const IconData chevron_right =
      IconData(61524, fontFamily: "fontawesome");
  static const IconData plus_circle =
      IconData(61525, fontFamily: "fontawesome");
  static const IconData minus_circle =
      IconData(61526, fontFamily: "fontawesome");
  static const IconData times_circle =
      IconData(61527, fontFamily: "fontawesome");
  static const IconData check_circle =
      IconData(61528, fontFamily: "fontawesome");
  static const IconData question_circle =
      IconData(61529, fontFamily: "fontawesome");
  static const IconData info_circle =
      IconData(61530, fontFamily: "fontawesome");
  static const IconData crosshairs = IconData(61531, fontFamily: "fontawesome");
  static const IconData times_circle_o =
      IconData(61532, fontFamily: "fontawesome");
  static const IconData check_circle_o =
      IconData(61533, fontFamily: "fontawesome");
  static const IconData ban = IconData(61534, fontFamily: "fontawesome");
  static const IconData arrow_left = IconData(61536, fontFamily: "fontawesome");
  static const IconData arrow_right =
      IconData(61537, fontFamily: "fontawesome");
  static const IconData arrow_up = IconData(61538, fontFamily: "fontawesome");
  static const IconData arrow_down = IconData(61539, fontFamily: "fontawesome");
  static const IconData mail_forward =
      IconData(61540, fontFamily: "fontawesome");
  static const IconData share = IconData(61540, fontFamily: "fontawesome");
  static const IconData expand = IconData(61541, fontFamily: "fontawesome");
  static const IconData compress = IconData(61542, fontFamily: "fontawesome");
  static const IconData plus = IconData(61543, fontFamily: "fontawesome");
  static const IconData minus = IconData(61544, fontFamily: "fontawesome");
  static const IconData asterisk = IconData(61545, fontFamily: "fontawesome");
  static const IconData exclamation_circle =
      IconData(61546, fontFamily: "fontawesome");
  static const IconData gift = IconData(61547, fontFamily: "fontawesome");
  static const IconData leaf = IconData(61548, fontFamily: "fontawesome");
  static const IconData fire = IconData(61549, fontFamily: "fontawesome");
  static const IconData eye = IconData(61550, fontFamily: "fontawesome");
  static const IconData eye_slash = IconData(61552, fontFamily: "fontawesome");
  static const IconData warning = IconData(61553, fontFamily: "fontawesome");
  static const IconData exclamation_triangle =
      IconData(61553, fontFamily: "fontawesome");
  static const IconData plane = IconData(61554, fontFamily: "fontawesome");
  static const IconData calendar = IconData(61555, fontFamily: "fontawesome");
  static const IconData random = IconData(61556, fontFamily: "fontawesome");
  static const IconData comment = IconData(61557, fontFamily: "fontawesome");
  static const IconData magnet = IconData(61558, fontFamily: "fontawesome");
  static const IconData chevron_up = IconData(61559, fontFamily: "fontawesome");
  static const IconData chevron_down =
      IconData(61560, fontFamily: "fontawesome");
  static const IconData retweet = IconData(61561, fontFamily: "fontawesome");
  static const IconData shopping_cart =
      IconData(61562, fontFamily: "fontawesome");
  static const IconData folder = IconData(61563, fontFamily: "fontawesome");
  static const IconData folder_open =
      IconData(61564, fontFamily: "fontawesome");
  static const IconData arrows_v = IconData(61565, fontFamily: "fontawesome");
  static const IconData arrows_h = IconData(61566, fontFamily: "fontawesome");
  static const IconData bar_chart_o =
      IconData(61568, fontFamily: "fontawesome");
  static const IconData bar_chart = IconData(61568, fontFamily: "fontawesome");
  static const IconData twitter_square =
      IconData(61569, fontFamily: "fontawesome");
  static const IconData facebook_square =
      IconData(61570, fontFamily: "fontawesome");
  static const IconData camera_retro =
      IconData(61571, fontFamily: "fontawesome");
  static const IconData key = IconData(61572, fontFamily: "fontawesome");
  static const IconData gears = IconData(61573, fontFamily: "fontawesome");
  static const IconData cogs = IconData(61573, fontFamily: "fontawesome");
  static const IconData comments = IconData(61574, fontFamily: "fontawesome");
  static const IconData thumbs_o_up =
      IconData(61575, fontFamily: "fontawesome");
  static const IconData thumbs_o_down =
      IconData(61576, fontFamily: "fontawesome");
  static const IconData star_half = IconData(61577, fontFamily: "fontawesome");
  static const IconData heart_o = IconData(61578, fontFamily: "fontawesome");
  static const IconData sign_out = IconData(61579, fontFamily: "fontawesome");
  static const IconData linkedin_square =
      IconData(61580, fontFamily: "fontawesome");
  static const IconData thumb_tack = IconData(61581, fontFamily: "fontawesome");
  static const IconData external_link =
      IconData(61582, fontFamily: "fontawesome");
  static const IconData sign_in = IconData(61584, fontFamily: "fontawesome");
  static const IconData trophy = IconData(61585, fontFamily: "fontawesome");
  static const IconData github_square =
      IconData(61586, fontFamily: "fontawesome");
  static const IconData upload = IconData(61587, fontFamily: "fontawesome");
  static const IconData lemon_o = IconData(61588, fontFamily: "fontawesome");
  static const IconData phone = IconData(61589, fontFamily: "fontawesome");
  static const IconData square_o = IconData(61590, fontFamily: "fontawesome");
  static const IconData bookmark_o = IconData(61591, fontFamily: "fontawesome");
  static const IconData phone_square =
      IconData(61592, fontFamily: "fontawesome");
  static const IconData twitter = IconData(61593, fontFamily: "fontawesome");
  static const IconData facebook_f = IconData(61594, fontFamily: "fontawesome");
  static const IconData facebook = IconData(61594, fontFamily: "fontawesome");
  static const IconData github = IconData(61595, fontFamily: "fontawesome");
  static const IconData unlock = IconData(61596, fontFamily: "fontawesome");
  static const IconData credit_card =
      IconData(61597, fontFamily: "fontawesome");
  static const IconData feed = IconData(61598, fontFamily: "fontawesome");
  static const IconData rss = IconData(61598, fontFamily: "fontawesome");
  static const IconData hdd_o = IconData(61600, fontFamily: "fontawesome");
  static const IconData bullhorn = IconData(61601, fontFamily: "fontawesome");
  static const IconData bell = IconData(61683, fontFamily: "fontawesome");
  static const IconData certificate =
      IconData(61603, fontFamily: "fontawesome");
  static const IconData hand_o_right =
      IconData(61604, fontFamily: "fontawesome");
  static const IconData hand_o_left =
      IconData(61605, fontFamily: "fontawesome");
  static const IconData hand_o_up = IconData(61606, fontFamily: "fontawesome");
  static const IconData hand_o_down =
      IconData(61607, fontFamily: "fontawesome");
  static const IconData arrow_circle_left =
      IconData(61608, fontFamily: "fontawesome");
  static const IconData arrow_circle_right =
      IconData(61609, fontFamily: "fontawesome");
  static const IconData arrow_circle_up =
      IconData(61610, fontFamily: "fontawesome");
  static const IconData arrow_circle_down =
      IconData(61611, fontFamily: "fontawesome");
  static const IconData globe = IconData(61612, fontFamily: "fontawesome");
  static const IconData wrench = IconData(61613, fontFamily: "fontawesome");
  static const IconData tasks = IconData(61614, fontFamily: "fontawesome");
  static const IconData filter = IconData(61616, fontFamily: "fontawesome");
  static const IconData briefcase = IconData(61617, fontFamily: "fontawesome");
  static const IconData arrows_alt = IconData(61618, fontFamily: "fontawesome");
  static const IconData group = IconData(61632, fontFamily: "fontawesome");
  static const IconData users = IconData(61632, fontFamily: "fontawesome");
  static const IconData chain = IconData(61633, fontFamily: "fontawesome");
  static const IconData link = IconData(61633, fontFamily: "fontawesome");
  static const IconData cloud = IconData(61634, fontFamily: "fontawesome");
  static const IconData flask = IconData(61635, fontFamily: "fontawesome");
  static const IconData cut = IconData(61636, fontFamily: "fontawesome");
  static const IconData scissors = IconData(61636, fontFamily: "fontawesome");
  static const IconData copy = IconData(61637, fontFamily: "fontawesome");
  static const IconData files_o = IconData(61637, fontFamily: "fontawesome");
  static const IconData paperclip = IconData(61638, fontFamily: "fontawesome");
  static const IconData save = IconData(61639, fontFamily: "fontawesome");
  static const IconData floppy_o = IconData(61639, fontFamily: "fontawesome");
  static const IconData square = IconData(61640, fontFamily: "fontawesome");
  static const IconData navicon = IconData(61641, fontFamily: "fontawesome");
  static const IconData reorder = IconData(61641, fontFamily: "fontawesome");
  static const IconData bars = IconData(61641, fontFamily: "fontawesome");
  static const IconData list_ul = IconData(61642, fontFamily: "fontawesome");
  static const IconData list_ol = IconData(61643, fontFamily: "fontawesome");
  static const IconData strikethrough =
      IconData(61644, fontFamily: "fontawesome");
  static const IconData underline = IconData(61645, fontFamily: "fontawesome");
  static const IconData table = IconData(61646, fontFamily: "fontawesome");
  static const IconData magic = IconData(61648, fontFamily: "fontawesome");
  static const IconData truck = IconData(61649, fontFamily: "fontawesome");
  static const IconData pinterest = IconData(61650, fontFamily: "fontawesome");
  static const IconData pinterest_square =
      IconData(61651, fontFamily: "fontawesome");
  static const IconData google_plus_square =
      IconData(61652, fontFamily: "fontawesome");
  static const IconData google_plus =
      IconData(61653, fontFamily: "fontawesome");
  static const IconData money = IconData(61654, fontFamily: "fontawesome");
  static const IconData caret_down = IconData(61655, fontFamily: "fontawesome");
  static const IconData caret_up = IconData(61656, fontFamily: "fontawesome");
  static const IconData caret_left = IconData(61657, fontFamily: "fontawesome");
  static const IconData caret_right =
      IconData(61658, fontFamily: "fontawesome");
  static const IconData columns = IconData(61659, fontFamily: "fontawesome");
  static const IconData unsorted = IconData(61660, fontFamily: "fontawesome");
  static const IconData sort = IconData(61660, fontFamily: "fontawesome");
  static const IconData sort_down = IconData(61661, fontFamily: "fontawesome");
  static const IconData sort_desc = IconData(61661, fontFamily: "fontawesome");
  static const IconData sort_up = IconData(61662, fontFamily: "fontawesome");
  static const IconData sort_asc = IconData(61662, fontFamily: "fontawesome");
  static const IconData envelope = IconData(61664, fontFamily: "fontawesome");
  static const IconData linkedin = IconData(61665, fontFamily: "fontawesome");
  static const IconData rotate_left =
      IconData(61666, fontFamily: "fontawesome");
  static const IconData undo = IconData(61666, fontFamily: "fontawesome");
  static const IconData legal = IconData(61667, fontFamily: "fontawesome");
  static const IconData gavel = IconData(61667, fontFamily: "fontawesome");
  static const IconData dashboard = IconData(61668, fontFamily: "fontawesome");
  static const IconData tachometer = IconData(61668, fontFamily: "fontawesome");
  static const IconData comment_o = IconData(61669, fontFamily: "fontawesome");
  static const IconData comments_o = IconData(61670, fontFamily: "fontawesome");
  static const IconData flash = IconData(61671, fontFamily: "fontawesome");
  static const IconData bolt = IconData(61671, fontFamily: "fontawesome");
  static const IconData sitemap = IconData(61672, fontFamily: "fontawesome");
  static const IconData umbrella = IconData(61673, fontFamily: "fontawesome");
  static const IconData paste = IconData(61674, fontFamily: "fontawesome");
  static const IconData clipboard = IconData(61674, fontFamily: "fontawesome");
  static const IconData lightbulb_o =
      IconData(61675, fontFamily: "fontawesome");
  static const IconData exchange = IconData(61676, fontFamily: "fontawesome");
  static const IconData cloud_download =
      IconData(61677, fontFamily: "fontawesome");
  static const IconData cloud_upload =
      IconData(61678, fontFamily: "fontawesome");
  static const IconData user_md = IconData(61680, fontFamily: "fontawesome");
  static const IconData stethoscope =
      IconData(61681, fontFamily: "fontawesome");
  static const IconData suitcase = IconData(61682, fontFamily: "fontawesome");
  static const IconData bell_o = IconData(61602, fontFamily: "fontawesome");
  static const IconData coffee = IconData(61684, fontFamily: "fontawesome");
  static const IconData cutlery = IconData(61685, fontFamily: "fontawesome");
  static const IconData file_text_o =
      IconData(61686, fontFamily: "fontawesome");
  static const IconData building_o = IconData(61687, fontFamily: "fontawesome");
  static const IconData hospital_o = IconData(61688, fontFamily: "fontawesome");
  static const IconData ambulance = IconData(61689, fontFamily: "fontawesome");
  static const IconData medkit = IconData(61690, fontFamily: "fontawesome");
  static const IconData fighter_jet =
      IconData(61691, fontFamily: "fontawesome");
  static const IconData beer = IconData(61692, fontFamily: "fontawesome");
  static const IconData h_square = IconData(61693, fontFamily: "fontawesome");
  static const IconData plus_square =
      IconData(61694, fontFamily: "fontawesome");
  static const IconData angle_double_left =
      IconData(61696, fontFamily: "fontawesome");
  static const IconData angle_double_right =
      IconData(61697, fontFamily: "fontawesome");
  static const IconData angle_double_up =
      IconData(61698, fontFamily: "fontawesome");
  static const IconData angle_double_down =
      IconData(61699, fontFamily: "fontawesome");
  static const IconData angle_left = IconData(61700, fontFamily: "fontawesome");
  static const IconData angle_right =
      IconData(61701, fontFamily: "fontawesome");
  static const IconData angle_up = IconData(61702, fontFamily: "fontawesome");
  static const IconData angle_down = IconData(61703, fontFamily: "fontawesome");
  static const IconData desktop = IconData(61704, fontFamily: "fontawesome");
  static const IconData laptop = IconData(61705, fontFamily: "fontawesome");
  static const IconData tablet = IconData(61706, fontFamily: "fontawesome");
  static const IconData mobile_phone =
      IconData(61707, fontFamily: "fontawesome");
  static const IconData mobile = IconData(61707, fontFamily: "fontawesome");
  static const IconData circle_o = IconData(61708, fontFamily: "fontawesome");
  static const IconData quote_left = IconData(61709, fontFamily: "fontawesome");
  static const IconData quote_right =
      IconData(61710, fontFamily: "fontawesome");
  static const IconData spinner = IconData(61712, fontFamily: "fontawesome");
  static const IconData circle = IconData(61713, fontFamily: "fontawesome");
  static const IconData mail_reply = IconData(61714, fontFamily: "fontawesome");
  static const IconData reply = IconData(61714, fontFamily: "fontawesome");
  static const IconData github_alt = IconData(61715, fontFamily: "fontawesome");
  static const IconData folder_o = IconData(61716, fontFamily: "fontawesome");
  static const IconData folder_open_o =
      IconData(61717, fontFamily: "fontawesome");
  static const IconData smile_o = IconData(61720, fontFamily: "fontawesome");
  static const IconData frown_o = IconData(61721, fontFamily: "fontawesome");
  static const IconData meh_o = IconData(61722, fontFamily: "fontawesome");
  static const IconData gamepad = IconData(61723, fontFamily: "fontawesome");
  static const IconData keyboard_o = IconData(61724, fontFamily: "fontawesome");
  static const IconData flag_o = IconData(61725, fontFamily: "fontawesome");
  static const IconData flag_checkered =
      IconData(61726, fontFamily: "fontawesome");
  static const IconData terminal = IconData(61728, fontFamily: "fontawesome");
  static const IconData code = IconData(61729, fontFamily: "fontawesome");
  static const IconData mail_reply_all =
      IconData(61730, fontFamily: "fontawesome");
  static const IconData reply_all = IconData(61730, fontFamily: "fontawesome");
  static const IconData star_half_empty =
      IconData(61731, fontFamily: "fontawesome");
  static const IconData star_half_full =
      IconData(61731, fontFamily: "fontawesome");
  static const IconData star_half_o =
      IconData(61731, fontFamily: "fontawesome");
  static const IconData location_arrow =
      IconData(61732, fontFamily: "fontawesome");
  static const IconData crop = IconData(61733, fontFamily: "fontawesome");
  static const IconData code_fork = IconData(61734, fontFamily: "fontawesome");
  static const IconData unlink = IconData(61735, fontFamily: "fontawesome");
  static const IconData chain_broken =
      IconData(61735, fontFamily: "fontawesome");
  static const IconData question = IconData(61736, fontFamily: "fontawesome");
  static const IconData info = IconData(61737, fontFamily: "fontawesome");
  static const IconData exclamation =
      IconData(61738, fontFamily: "fontawesome");
  static const IconData superscript =
      IconData(61739, fontFamily: "fontawesome");
  static const IconData subscript = IconData(61740, fontFamily: "fontawesome");
  static const IconData eraser = IconData(61741, fontFamily: "fontawesome");
  static const IconData puzzle_piece =
      IconData(61742, fontFamily: "fontawesome");
  static const IconData microphone = IconData(61744, fontFamily: "fontawesome");
  static const IconData microphone_slash =
      IconData(61745, fontFamily: "fontawesome");
  static const IconData shield = IconData(61746, fontFamily: "fontawesome");
  static const IconData calendar_o = IconData(61747, fontFamily: "fontawesome");
  static const IconData fire_extinguisher =
      IconData(61748, fontFamily: "fontawesome");
  static const IconData rocket = IconData(61749, fontFamily: "fontawesome");
  static const IconData maxcdn = IconData(61750, fontFamily: "fontawesome");
  static const IconData chevron_circle_left =
      IconData(61751, fontFamily: "fontawesome");
  static const IconData chevron_circle_right =
      IconData(61752, fontFamily: "fontawesome");
  static const IconData chevron_circle_up =
      IconData(61753, fontFamily: "fontawesome");
  static const IconData chevron_circle_down =
      IconData(61754, fontFamily: "fontawesome");
  static const IconData html5 = IconData(61755, fontFamily: "fontawesome");
  static const IconData css3 = IconData(61756, fontFamily: "fontawesome");
  static const IconData anchor = IconData(61757, fontFamily: "fontawesome");
  static const IconData unlock_alt = IconData(61758, fontFamily: "fontawesome");
  static const IconData bullseye = IconData(61760, fontFamily: "fontawesome");
  static const IconData ellipsis_h = IconData(61761, fontFamily: "fontawesome");
  static const IconData ellipsis_v = IconData(61762, fontFamily: "fontawesome");
  static const IconData rss_square = IconData(61763, fontFamily: "fontawesome");
  static const IconData play_circle =
      IconData(61764, fontFamily: "fontawesome");
  static const IconData ticket = IconData(61765, fontFamily: "fontawesome");
  static const IconData minus_square =
      IconData(61766, fontFamily: "fontawesome");
  static const IconData minus_square_o =
      IconData(61767, fontFamily: "fontawesome");
  static const IconData level_up = IconData(61768, fontFamily: "fontawesome");
  static const IconData level_down = IconData(61769, fontFamily: "fontawesome");
  static const IconData check_square =
      IconData(61770, fontFamily: "fontawesome");
  static const IconData pencil_square =
      IconData(61771, fontFamily: "fontawesome");
  static const IconData external_link_square =
      IconData(61772, fontFamily: "fontawesome");
  static const IconData share_square =
      IconData(61773, fontFamily: "fontawesome");
  static const IconData compass = IconData(61774, fontFamily: "fontawesome");
  static const IconData toggle_down =
      IconData(61776, fontFamily: "fontawesome");
  static const IconData caret_square_o_down =
      IconData(61776, fontFamily: "fontawesome");
  static const IconData toggle_up = IconData(61777, fontFamily: "fontawesome");
  static const IconData caret_square_o_up =
      IconData(61777, fontFamily: "fontawesome");
  static const IconData toggle_right =
      IconData(61778, fontFamily: "fontawesome");
  static const IconData caret_square_o_right =
      IconData(61778, fontFamily: "fontawesome");
  static const IconData euro = IconData(61779, fontFamily: "fontawesome");
  static const IconData eur = IconData(61779, fontFamily: "fontawesome");
  static const IconData gbp = IconData(61780, fontFamily: "fontawesome");
  static const IconData dollar = IconData(61781, fontFamily: "fontawesome");
  static const IconData usd = IconData(61781, fontFamily: "fontawesome");
  static const IconData rupee = IconData(61782, fontFamily: "fontawesome");
  static const IconData inr = IconData(61782, fontFamily: "fontawesome");
  static const IconData cny = IconData(61783, fontFamily: "fontawesome");
  static const IconData rmb = IconData(61783, fontFamily: "fontawesome");
  static const IconData yen = IconData(61783, fontFamily: "fontawesome");
  static const IconData jpy = IconData(61783, fontFamily: "fontawesome");
  static const IconData ruble = IconData(61784, fontFamily: "fontawesome");
  static const IconData rouble = IconData(61784, fontFamily: "fontawesome");
  static const IconData rub = IconData(61784, fontFamily: "fontawesome");
  static const IconData won = IconData(61785, fontFamily: "fontawesome");
  static const IconData krw = IconData(61785, fontFamily: "fontawesome");
  static const IconData bitcoin = IconData(61786, fontFamily: "fontawesome");
  static const IconData btc = IconData(61786, fontFamily: "fontawesome");
  static const IconData file = IconData(61787, fontFamily: "fontawesome");
  static const IconData file_text = IconData(61788, fontFamily: "fontawesome");
  static const IconData sort_alpha_asc =
      IconData(61789, fontFamily: "fontawesome");
  static const IconData sort_alpha_desc =
      IconData(61790, fontFamily: "fontawesome");
  static const IconData sort_amount_asc =
      IconData(61792, fontFamily: "fontawesome");
  static const IconData sort_amount_desc =
      IconData(61793, fontFamily: "fontawesome");
  static const IconData sort_numeric_asc =
      IconData(61794, fontFamily: "fontawesome");
  static const IconData sort_numeric_desc =
      IconData(61795, fontFamily: "fontawesome");
  static const IconData thumbs_up = IconData(61796, fontFamily: "fontawesome");
  static const IconData thumbs_down =
      IconData(61797, fontFamily: "fontawesome");
  static const IconData youtube_square =
      IconData(61798, fontFamily: "fontawesome");
  static const IconData youtube = IconData(61799, fontFamily: "fontawesome");
  static const IconData xing = IconData(61800, fontFamily: "fontawesome");
  static const IconData xing_square =
      IconData(61801, fontFamily: "fontawesome");
  static const IconData youtube_play =
      IconData(61802, fontFamily: "fontawesome");
  static const IconData dropbox = IconData(61803, fontFamily: "fontawesome");
  static const IconData stack_overflow =
      IconData(61804, fontFamily: "fontawesome");
  static const IconData instagram = IconData(61805, fontFamily: "fontawesome");
  static const IconData flickr = IconData(61806, fontFamily: "fontawesome");
  static const IconData adn = IconData(61808, fontFamily: "fontawesome");
  static const IconData bitbucket = IconData(61809, fontFamily: "fontawesome");
  static const IconData bitbucket_square =
      IconData(61810, fontFamily: "fontawesome");
  static const IconData tumblr = IconData(61811, fontFamily: "fontawesome");
  static const IconData tumblr_square =
      IconData(61812, fontFamily: "fontawesome");
  static const IconData long_arrow_down =
      IconData(61813, fontFamily: "fontawesome");
  static const IconData long_arrow_up =
      IconData(61814, fontFamily: "fontawesome");
  static const IconData long_arrow_left =
      IconData(61815, fontFamily: "fontawesome");
  static const IconData long_arrow_right =
      IconData(61816, fontFamily: "fontawesome");
  static const IconData apple = IconData(61817, fontFamily: "fontawesome");
  static const IconData windows = IconData(61818, fontFamily: "fontawesome");
  static const IconData android = IconData(61819, fontFamily: "fontawesome");
  static const IconData linux = IconData(61820, fontFamily: "fontawesome");
  static const IconData dribbble = IconData(61821, fontFamily: "fontawesome");
  static const IconData skype = IconData(61822, fontFamily: "fontawesome");
  static const IconData foursquare = IconData(61824, fontFamily: "fontawesome");
  static const IconData trello = IconData(61825, fontFamily: "fontawesome");
  static const IconData female = IconData(61826, fontFamily: "fontawesome");
  static const IconData male = IconData(61827, fontFamily: "fontawesome");
  static const IconData gittip = IconData(61828, fontFamily: "fontawesome");
  static const IconData gratipay = IconData(61828, fontFamily: "fontawesome");
  static const IconData sun_o = IconData(61829, fontFamily: "fontawesome");
  static const IconData moon_o = IconData(61830, fontFamily: "fontawesome");
  static const IconData archive = IconData(61831, fontFamily: "fontawesome");
  static const IconData bug = IconData(61832, fontFamily: "fontawesome");
  static const IconData vk = IconData(61833, fontFamily: "fontawesome");
  static const IconData weibo = IconData(61834, fontFamily: "fontawesome");
  static const IconData renren = IconData(61835, fontFamily: "fontawesome");
  static const IconData pagelines = IconData(61836, fontFamily: "fontawesome");
  static const IconData stack_exchange =
      IconData(61837, fontFamily: "fontawesome");
  static const IconData arrow_circle_o_right =
      IconData(61838, fontFamily: "fontawesome");
  static const IconData arrow_circle_o_left =
      IconData(61840, fontFamily: "fontawesome");
  static const IconData toggle_left =
      IconData(61841, fontFamily: "fontawesome");
  static const IconData caret_square_o_left =
      IconData(61841, fontFamily: "fontawesome");
  static const IconData dot_circle_o =
      IconData(61842, fontFamily: "fontawesome");
  static const IconData wheelchair = IconData(61843, fontFamily: "fontawesome");
  static const IconData vimeo_square =
      IconData(61844, fontFamily: "fontawesome");
  static const IconData turkish_lira =
      IconData(61845, fontFamily: "fontawesome");
  static const IconData try_icon = IconData(61845, fontFamily: "fontawesome");
  static const IconData plus_square_o =
      IconData(61846, fontFamily: "fontawesome");
  static const IconData space_shuttle =
      IconData(61847, fontFamily: "fontawesome");
  static const IconData slack = IconData(61848, fontFamily: "fontawesome");
  static const IconData envelope_square =
      IconData(61849, fontFamily: "fontawesome");
  static const IconData wordpress = IconData(61850, fontFamily: "fontawesome");
  static const IconData openid = IconData(61851, fontFamily: "fontawesome");
  static const IconData institution =
      IconData(61852, fontFamily: "fontawesome");
  static const IconData bank = IconData(61852, fontFamily: "fontawesome");
  static const IconData university = IconData(61852, fontFamily: "fontawesome");
  static const IconData mortar_board =
      IconData(61853, fontFamily: "fontawesome");
  static const IconData graduation_cap =
      IconData(61853, fontFamily: "fontawesome");
  static const IconData yahoo = IconData(61854, fontFamily: "fontawesome");
  static const IconData google = IconData(61856, fontFamily: "fontawesome");
  static const IconData reddit = IconData(61857, fontFamily: "fontawesome");
  static const IconData reddit_square =
      IconData(61858, fontFamily: "fontawesome");
  static const IconData stumbleupon_circle =
      IconData(61859, fontFamily: "fontawesome");
  static const IconData stumbleupon =
      IconData(61860, fontFamily: "fontawesome");
  static const IconData delicious = IconData(61861, fontFamily: "fontawesome");
  static const IconData digg = IconData(61862, fontFamily: "fontawesome");
  static const IconData pied_piper_pp =
      IconData(61863, fontFamily: "fontawesome");
  static const IconData pied_piper_alt =
      IconData(61864, fontFamily: "fontawesome");
  static const IconData drupal = IconData(61865, fontFamily: "fontawesome");
  static const IconData joomla = IconData(61866, fontFamily: "fontawesome");
  static const IconData language = IconData(61867, fontFamily: "fontawesome");
  static const IconData fax = IconData(61868, fontFamily: "fontawesome");
  static const IconData building = IconData(61869, fontFamily: "fontawesome");
  static const IconData child = IconData(61870, fontFamily: "fontawesome");
  static const IconData paw = IconData(61872, fontFamily: "fontawesome");
  static const IconData spoon = IconData(61873, fontFamily: "fontawesome");
  static const IconData cube = IconData(61874, fontFamily: "fontawesome");
  static const IconData cubes = IconData(61875, fontFamily: "fontawesome");
  static const IconData behance = IconData(61876, fontFamily: "fontawesome");
  static const IconData behance_square =
      IconData(61877, fontFamily: "fontawesome");
  static const IconData steam = IconData(61878, fontFamily: "fontawesome");
  static const IconData steam_square =
      IconData(61879, fontFamily: "fontawesome");
  static const IconData recycle = IconData(61880, fontFamily: "fontawesome");
  static const IconData automobile = IconData(61881, fontFamily: "fontawesome");
  static const IconData car = IconData(61881, fontFamily: "fontawesome");
  static const IconData cab = IconData(61882, fontFamily: "fontawesome");
  static const IconData taxi = IconData(61882, fontFamily: "fontawesome");
  static const IconData tree = IconData(61883, fontFamily: "fontawesome");
  static const IconData spotify = IconData(61884, fontFamily: "fontawesome");
  static const IconData deviantart = IconData(61885, fontFamily: "fontawesome");
  static const IconData soundcloud = IconData(61886, fontFamily: "fontawesome");
  static const IconData database = IconData(61888, fontFamily: "fontawesome");
  static const IconData file_pdf_o = IconData(61889, fontFamily: "fontawesome");
  static const IconData file_word_o =
      IconData(61890, fontFamily: "fontawesome");
  static const IconData file_excel_o =
      IconData(61891, fontFamily: "fontawesome");
  static const IconData file_powerpoint_o =
      IconData(61892, fontFamily: "fontawesome");
  static const IconData file_photo_o =
      IconData(61893, fontFamily: "fontawesome");
  static const IconData file_picture_o =
      IconData(61893, fontFamily: "fontawesome");
  static const IconData file_image_o =
      IconData(61893, fontFamily: "fontawesome");
  static const IconData file_zip_o = IconData(61894, fontFamily: "fontawesome");
  static const IconData file_archive_o =
      IconData(61894, fontFamily: "fontawesome");
  static const IconData file_sound_o =
      IconData(61895, fontFamily: "fontawesome");
  static const IconData file_audio_o =
      IconData(61895, fontFamily: "fontawesome");
  static const IconData file_movie_o =
      IconData(61896, fontFamily: "fontawesome");
  static const IconData file_video_o =
      IconData(61896, fontFamily: "fontawesome");
  static const IconData file_code_o =
      IconData(61897, fontFamily: "fontawesome");
  static const IconData vine = IconData(61898, fontFamily: "fontawesome");
  static const IconData codepen = IconData(61899, fontFamily: "fontawesome");
  static const IconData jsfiddle = IconData(61900, fontFamily: "fontawesome");
  static const IconData life_bouy = IconData(61901, fontFamily: "fontawesome");
  static const IconData life_buoy = IconData(61901, fontFamily: "fontawesome");
  static const IconData life_saver = IconData(61901, fontFamily: "fontawesome");
  static const IconData support = IconData(61901, fontFamily: "fontawesome");
  static const IconData life_ring = IconData(61901, fontFamily: "fontawesome");
  static const IconData circle_o_notch =
      IconData(61902, fontFamily: "fontawesome");
  static const IconData ra = IconData(61904, fontFamily: "fontawesome");
  static const IconData resistance = IconData(61904, fontFamily: "fontawesome");
  static const IconData rebel = IconData(61904, fontFamily: "fontawesome");
  static const IconData ge = IconData(61905, fontFamily: "fontawesome");
  static const IconData empire = IconData(61905, fontFamily: "fontawesome");
  static const IconData git_square = IconData(61906, fontFamily: "fontawesome");
  static const IconData git = IconData(61907, fontFamily: "fontawesome");
  static const IconData y_combinator_square =
      IconData(61908, fontFamily: "fontawesome");
  static const IconData yc_square = IconData(61908, fontFamily: "fontawesome");
  static const IconData hacker_news =
      IconData(61908, fontFamily: "fontawesome");
  static const IconData tencent_weibo =
      IconData(61909, fontFamily: "fontawesome");
  static const IconData qq = IconData(61910, fontFamily: "fontawesome");
  static const IconData wechat = IconData(61911, fontFamily: "fontawesome");
  static const IconData weixin = IconData(61911, fontFamily: "fontawesome");
  static const IconData send = IconData(61912, fontFamily: "fontawesome");
  static const IconData paper_plane =
      IconData(61912, fontFamily: "fontawesome");
  static const IconData send_o = IconData(61913, fontFamily: "fontawesome");
  static const IconData paper_plane_o =
      IconData(61913, fontFamily: "fontawesome");
  static const IconData history = IconData(61914, fontFamily: "fontawesome");
  static const IconData circle_thin =
      IconData(61915, fontFamily: "fontawesome");
  static const IconData header = IconData(61916, fontFamily: "fontawesome");
  static const IconData paragraph = IconData(61917, fontFamily: "fontawesome");
  static const IconData sliders = IconData(61918, fontFamily: "fontawesome");
  static const IconData share_alt = IconData(61920, fontFamily: "fontawesome");
  static const IconData share_alt_square =
      IconData(61921, fontFamily: "fontawesome");
  static const IconData bomb = IconData(61922, fontFamily: "fontawesome");
  static const IconData soccer_ball_o =
      IconData(61923, fontFamily: "fontawesome");
  static const IconData futbol_o = IconData(61923, fontFamily: "fontawesome");
  static const IconData tty = IconData(61924, fontFamily: "fontawesome");
  static const IconData binoculars = IconData(61925, fontFamily: "fontawesome");
  static const IconData plug = IconData(61926, fontFamily: "fontawesome");
  static const IconData slideshare = IconData(61927, fontFamily: "fontawesome");
  static const IconData twitch = IconData(61928, fontFamily: "fontawesome");
  static const IconData yelp = IconData(61929, fontFamily: "fontawesome");
  static const IconData newspaper_o =
      IconData(61930, fontFamily: "fontawesome");
  static const IconData wifi = IconData(61931, fontFamily: "fontawesome");
  static const IconData calculator = IconData(61932, fontFamily: "fontawesome");
  static const IconData paypal = IconData(61933, fontFamily: "fontawesome");
  static const IconData google_wallet =
      IconData(61934, fontFamily: "fontawesome");
  static const IconData cc_visa = IconData(61936, fontFamily: "fontawesome");
  static const IconData cc_mastercard =
      IconData(61937, fontFamily: "fontawesome");
  static const IconData cc_discover =
      IconData(61938, fontFamily: "fontawesome");
  static const IconData cc_amex = IconData(61939, fontFamily: "fontawesome");
  static const IconData cc_paypal = IconData(61940, fontFamily: "fontawesome");
  static const IconData cc_stripe = IconData(61941, fontFamily: "fontawesome");
  static const IconData bell_slash = IconData(61942, fontFamily: "fontawesome");
  static const IconData bell_slash_o =
      IconData(61943, fontFamily: "fontawesome");
  static const IconData trash = IconData(61944, fontFamily: "fontawesome");
  static const IconData copyright = IconData(61945, fontFamily: "fontawesome");
  static const IconData at = IconData(61946, fontFamily: "fontawesome");
  static const IconData eyedropper = IconData(61947, fontFamily: "fontawesome");
  static const IconData paint_brush =
      IconData(61948, fontFamily: "fontawesome");
  static const IconData birthday_cake =
      IconData(61949, fontFamily: "fontawesome");
  static const IconData area_chart = IconData(61950, fontFamily: "fontawesome");
  static const IconData pie_chart = IconData(61952, fontFamily: "fontawesome");
  static const IconData line_chart = IconData(61953, fontFamily: "fontawesome");
  static const IconData lastfm = IconData(61954, fontFamily: "fontawesome");
  static const IconData lastfm_square =
      IconData(61955, fontFamily: "fontawesome");
  static const IconData toggle_off = IconData(61956, fontFamily: "fontawesome");
  static const IconData toggle_on = IconData(61957, fontFamily: "fontawesome");
  static const IconData bicycle = IconData(61958, fontFamily: "fontawesome");
  static const IconData bus = IconData(61959, fontFamily: "fontawesome");
  static const IconData ioxhost = IconData(61960, fontFamily: "fontawesome");
  static const IconData angellist = IconData(61961, fontFamily: "fontawesome");
  static const IconData cc = IconData(61962, fontFamily: "fontawesome");
  static const IconData shekel = IconData(61963, fontFamily: "fontawesome");
  static const IconData sheqel = IconData(61963, fontFamily: "fontawesome");
  static const IconData ils = IconData(61963, fontFamily: "fontawesome");
  static const IconData meanpath = IconData(61964, fontFamily: "fontawesome");
  static const IconData buysellads = IconData(61965, fontFamily: "fontawesome");
  static const IconData connectdevelop =
      IconData(61966, fontFamily: "fontawesome");
  static const IconData dashcube = IconData(61968, fontFamily: "fontawesome");
  static const IconData forumbee = IconData(61969, fontFamily: "fontawesome");
  static const IconData leanpub = IconData(61970, fontFamily: "fontawesome");
  static const IconData sellsy = IconData(61971, fontFamily: "fontawesome");
  static const IconData shirtsinbulk =
      IconData(61972, fontFamily: "fontawesome");
  static const IconData simplybuilt =
      IconData(61973, fontFamily: "fontawesome");
  static const IconData skyatlas = IconData(61974, fontFamily: "fontawesome");
  static const IconData cart_plus = IconData(61975, fontFamily: "fontawesome");
  static const IconData cart_arrow_down =
      IconData(61976, fontFamily: "fontawesome");
  static const IconData diamond = IconData(61977, fontFamily: "fontawesome");
  static const IconData ship = IconData(61978, fontFamily: "fontawesome");
  static const IconData user_secret =
      IconData(61979, fontFamily: "fontawesome");
  static const IconData motorcycle = IconData(61980, fontFamily: "fontawesome");
  static const IconData street_view =
      IconData(61981, fontFamily: "fontawesome");
  static const IconData heartbeat = IconData(61982, fontFamily: "fontawesome");
  static const IconData venus = IconData(61985, fontFamily: "fontawesome");
  static const IconData mars = IconData(61986, fontFamily: "fontawesome");
  static const IconData mercury = IconData(61987, fontFamily: "fontawesome");
  static const IconData intersex = IconData(61988, fontFamily: "fontawesome");
  static const IconData transgender =
      IconData(61988, fontFamily: "fontawesome");
  static const IconData transgender_alt =
      IconData(61989, fontFamily: "fontawesome");
  static const IconData venus_double =
      IconData(61990, fontFamily: "fontawesome");
  static const IconData mars_double =
      IconData(61991, fontFamily: "fontawesome");
  static const IconData venus_mars = IconData(61992, fontFamily: "fontawesome");
  static const IconData mars_stroke =
      IconData(61993, fontFamily: "fontawesome");
  static const IconData mars_stroke_v =
      IconData(61994, fontFamily: "fontawesome");
  static const IconData mars_stroke_h =
      IconData(61995, fontFamily: "fontawesome");
  static const IconData neuter = IconData(61996, fontFamily: "fontawesome");
  static const IconData genderless = IconData(61997, fontFamily: "fontawesome");
  static const IconData facebook_official =
      IconData(62000, fontFamily: "fontawesome");
  static const IconData pinterest_p =
      IconData(62001, fontFamily: "fontawesome");
  static const IconData whatsapp = IconData(62002, fontFamily: "fontawesome");
  static const IconData server = IconData(62003, fontFamily: "fontawesome");
  static const IconData user_plus = IconData(62004, fontFamily: "fontawesome");
  static const IconData user_times = IconData(62005, fontFamily: "fontawesome");
  static const IconData hotel = IconData(62006, fontFamily: "fontawesome");
  static const IconData bed = IconData(62006, fontFamily: "fontawesome");
  static const IconData viacoin = IconData(62007, fontFamily: "fontawesome");
  static const IconData train = IconData(62008, fontFamily: "fontawesome");
  static const IconData subway = IconData(62009, fontFamily: "fontawesome");
  static const IconData medium = IconData(62010, fontFamily: "fontawesome");
  static const IconData yc = IconData(62011, fontFamily: "fontawesome");
  static const IconData y_combinator =
      IconData(62011, fontFamily: "fontawesome");
  static const IconData optin_monster =
      IconData(62012, fontFamily: "fontawesome");
  static const IconData opencart = IconData(62013, fontFamily: "fontawesome");
  static const IconData expeditedssl =
      IconData(62014, fontFamily: "fontawesome");
  static const IconData battery_4 = IconData(62016, fontFamily: "fontawesome");
  static const IconData battery = IconData(62016, fontFamily: "fontawesome");
  static const IconData battery_full =
      IconData(62016, fontFamily: "fontawesome");
  static const IconData battery_3 = IconData(62017, fontFamily: "fontawesome");
  static const IconData battery_three_quarters =
      IconData(62017, fontFamily: "fontawesome");
  static const IconData battery_2 = IconData(62018, fontFamily: "fontawesome");
  static const IconData battery_half =
      IconData(62018, fontFamily: "fontawesome");
  static const IconData battery_1 = IconData(62019, fontFamily: "fontawesome");
  static const IconData battery_quarter =
      IconData(62019, fontFamily: "fontawesome");
  static const IconData battery_0 = IconData(62020, fontFamily: "fontawesome");
  static const IconData battery_empty =
      IconData(62020, fontFamily: "fontawesome");
  static const IconData mouse_pointer =
      IconData(62021, fontFamily: "fontawesome");
  static const IconData i_cursor = IconData(62022, fontFamily: "fontawesome");
  static const IconData object_group =
      IconData(62023, fontFamily: "fontawesome");
  static const IconData object_ungroup =
      IconData(62024, fontFamily: "fontawesome");
  static const IconData sticky_note =
      IconData(62025, fontFamily: "fontawesome");
  static const IconData sticky_note_o =
      IconData(62026, fontFamily: "fontawesome");
  static const IconData cc_jcb = IconData(62027, fontFamily: "fontawesome");
  static const IconData cc_diners_club =
      IconData(62028, fontFamily: "fontawesome");
  static const IconData clone = IconData(62029, fontFamily: "fontawesome");
  static const IconData balance_scale =
      IconData(62030, fontFamily: "fontawesome");
  static const IconData hourglass_o =
      IconData(62032, fontFamily: "fontawesome");
  static const IconData hourglass_1 =
      IconData(62033, fontFamily: "fontawesome");
  static const IconData hourglass_start =
      IconData(62033, fontFamily: "fontawesome");
  static const IconData hourglass_2 =
      IconData(62034, fontFamily: "fontawesome");
  static const IconData hourglass_half =
      IconData(62034, fontFamily: "fontawesome");
  static const IconData hourglass_3 =
      IconData(62035, fontFamily: "fontawesome");
  static const IconData hourglass_end =
      IconData(62035, fontFamily: "fontawesome");
  static const IconData hourglass = IconData(62036, fontFamily: "fontawesome");
  static const IconData hand_grab_o =
      IconData(62037, fontFamily: "fontawesome");
  static const IconData hand_rock_o =
      IconData(62037, fontFamily: "fontawesome");
  static const IconData hand_stop_o =
      IconData(62038, fontFamily: "fontawesome");
  static const IconData hand_paper_o =
      IconData(62038, fontFamily: "fontawesome");
  static const IconData hand_scissors_o =
      IconData(62039, fontFamily: "fontawesome");
  static const IconData hand_lizard_o =
      IconData(62040, fontFamily: "fontawesome");
  static const IconData hand_spock_o =
      IconData(62041, fontFamily: "fontawesome");
  static const IconData hand_pointer_o =
      IconData(62042, fontFamily: "fontawesome");
  static const IconData hand_peace_o =
      IconData(62043, fontFamily: "fontawesome");
  static const IconData trademark = IconData(62044, fontFamily: "fontawesome");
  static const IconData registered = IconData(62045, fontFamily: "fontawesome");
  static const IconData creative_commons =
      IconData(62046, fontFamily: "fontawesome");
  static const IconData gg = IconData(62048, fontFamily: "fontawesome");
  static const IconData gg_circle = IconData(62049, fontFamily: "fontawesome");
  static const IconData tripadvisor =
      IconData(62050, fontFamily: "fontawesome");
  static const IconData odnoklassniki =
      IconData(62051, fontFamily: "fontawesome");
  static const IconData odnoklassniki_square =
      IconData(62052, fontFamily: "fontawesome");
  static const IconData get_pocket = IconData(62053, fontFamily: "fontawesome");
  static const IconData wikipedia_w =
      IconData(62054, fontFamily: "fontawesome");
  static const IconData safari = IconData(62055, fontFamily: "fontawesome");
  static const IconData chrome = IconData(62056, fontFamily: "fontawesome");
  static const IconData firefox = IconData(62057, fontFamily: "fontawesome");
  static const IconData opera = IconData(62058, fontFamily: "fontawesome");
  static const IconData internet_explorer =
      IconData(62059, fontFamily: "fontawesome");
  static const IconData tv = IconData(62060, fontFamily: "fontawesome");
  static const IconData television = IconData(62060, fontFamily: "fontawesome");
  static const IconData contao = IconData(62061, fontFamily: "fontawesome");
  static const IconData icon_500px = IconData(62062, fontFamily: "fontawesome");
  static const IconData amazon = IconData(62064, fontFamily: "fontawesome");
  static const IconData calendar_plus_o =
      IconData(62065, fontFamily: "fontawesome");
  static const IconData calendar_minus_o =
      IconData(62066, fontFamily: "fontawesome");
  static const IconData calendar_times_o =
      IconData(62067, fontFamily: "fontawesome");
  static const IconData calendar_check_o =
      IconData(62068, fontFamily: "fontawesome");
  static const IconData industry = IconData(62069, fontFamily: "fontawesome");
  static const IconData map_pin = IconData(62070, fontFamily: "fontawesome");
  static const IconData map_signs = IconData(62071, fontFamily: "fontawesome");
  static const IconData map_o = IconData(62072, fontFamily: "fontawesome");
  static const IconData map = IconData(62073, fontFamily: "fontawesome");
  static const IconData commenting = IconData(62074, fontFamily: "fontawesome");
  static const IconData commenting_o =
      IconData(62075, fontFamily: "fontawesome");
  static const IconData houzz = IconData(62076, fontFamily: "fontawesome");
  static const IconData vimeo = IconData(62077, fontFamily: "fontawesome");
  static const IconData black_tie = IconData(62078, fontFamily: "fontawesome");
  static const IconData fonticons = IconData(62080, fontFamily: "fontawesome");
  static const IconData reddit_alien =
      IconData(62081, fontFamily: "fontawesome");
  static const IconData edge = IconData(62082, fontFamily: "fontawesome");
  static const IconData credit_card_alt =
      IconData(62083, fontFamily: "fontawesome");
  static const IconData codiepie = IconData(62084, fontFamily: "fontawesome");
  static const IconData modx = IconData(62085, fontFamily: "fontawesome");
  static const IconData fort_awesome =
      IconData(62086, fontFamily: "fontawesome");
  static const IconData usb = IconData(62087, fontFamily: "fontawesome");
  static const IconData product_hunt =
      IconData(62088, fontFamily: "fontawesome");
  static const IconData mixcloud = IconData(62089, fontFamily: "fontawesome");
  static const IconData scribd = IconData(62090, fontFamily: "fontawesome");
  static const IconData pause_circle =
      IconData(62091, fontFamily: "fontawesome");
  static const IconData pause_circle_o =
      IconData(62092, fontFamily: "fontawesome");
  static const IconData stop_circle =
      IconData(62093, fontFamily: "fontawesome");
  static const IconData stop_circle_o =
      IconData(62094, fontFamily: "fontawesome");
  static const IconData shopping_bag =
      IconData(62096, fontFamily: "fontawesome");
  static const IconData shopping_basket =
      IconData(62097, fontFamily: "fontawesome");
  static const IconData hashtag = IconData(62098, fontFamily: "fontawesome");
  static const IconData bluetooth = IconData(62099, fontFamily: "fontawesome");
  static const IconData bluetooth_b =
      IconData(62100, fontFamily: "fontawesome");
  static const IconData percent = IconData(62101, fontFamily: "fontawesome");
  static const IconData gitlab = IconData(62102, fontFamily: "fontawesome");
  static const IconData wpbeginner = IconData(62103, fontFamily: "fontawesome");
  static const IconData wpforms = IconData(62104, fontFamily: "fontawesome");
  static const IconData envira = IconData(62105, fontFamily: "fontawesome");
  static const IconData universal_access =
      IconData(62106, fontFamily: "fontawesome");
  static const IconData wheelchair_alt =
      IconData(62107, fontFamily: "fontawesome");
  static const IconData question_circle_o =
      IconData(62108, fontFamily: "fontawesome");
  static const IconData blind = IconData(62109, fontFamily: "fontawesome");
  static const IconData audio_description =
      IconData(62110, fontFamily: "fontawesome");
  static const IconData volume_control_phone =
      IconData(62112, fontFamily: "fontawesome");
  static const IconData braille = IconData(62113, fontFamily: "fontawesome");
  static const IconData assistive_listening_systems =
      IconData(62114, fontFamily: "fontawesome");
  static const IconData asl_interpreting =
      IconData(62115, fontFamily: "fontawesome");
  static const IconData american_sign_language_interpreting =
      IconData(62115, fontFamily: "fontawesome");
  static const IconData deafness = IconData(62116, fontFamily: "fontawesome");
  static const IconData hard_of_hearing =
      IconData(62116, fontFamily: "fontawesome");
  static const IconData deaf = IconData(62116, fontFamily: "fontawesome");
  static const IconData glide = IconData(62117, fontFamily: "fontawesome");
  static const IconData glide_g = IconData(62118, fontFamily: "fontawesome");
  static const IconData signing = IconData(62119, fontFamily: "fontawesome");
  static const IconData sign_language =
      IconData(62119, fontFamily: "fontawesome");
  static const IconData low_vision = IconData(62120, fontFamily: "fontawesome");
  static const IconData viadeo = IconData(62121, fontFamily: "fontawesome");
  static const IconData viadeo_square =
      IconData(62122, fontFamily: "fontawesome");
  static const IconData snapchat = IconData(62123, fontFamily: "fontawesome");
  static const IconData snapchat_ghost =
      IconData(62124, fontFamily: "fontawesome");
  static const IconData snapchat_square =
      IconData(62125, fontFamily: "fontawesome");
  static const IconData pied_piper = IconData(62126, fontFamily: "fontawesome");
  static const IconData first_order =
      IconData(62128, fontFamily: "fontawesome");
  static const IconData yoast = IconData(62129, fontFamily: "fontawesome");
  static const IconData themeisle = IconData(62130, fontFamily: "fontawesome");
  static const IconData google_plus_circle =
      IconData(62131, fontFamily: "fontawesome");
  static const IconData google_plus_official =
      IconData(62131, fontFamily: "fontawesome");
  static const IconData fa = IconData(62132, fontFamily: "fontawesome");
  static const IconData font_awesome =
      IconData(62132, fontFamily: "fontawesome");
  static const IconData handshake_o =
      IconData(62133, fontFamily: "fontawesome");
  static const IconData envelope_open =
      IconData(62134, fontFamily: "fontawesome");
  static const IconData envelope_open_o =
      IconData(62135, fontFamily: "fontawesome");
  static const IconData linode = IconData(62136, fontFamily: "fontawesome");
  static const IconData address_book =
      IconData(62137, fontFamily: "fontawesome");
  static const IconData address_book_o =
      IconData(62138, fontFamily: "fontawesome");
  static const IconData vcard = IconData(62139, fontFamily: "fontawesome");
  static const IconData address_card =
      IconData(62139, fontFamily: "fontawesome");
  static const IconData vcard_o = IconData(62140, fontFamily: "fontawesome");
  static const IconData address_card_o =
      IconData(62140, fontFamily: "fontawesome");
  static const IconData user_circle =
      IconData(62141, fontFamily: "fontawesome");
  static const IconData user_circle_o =
      IconData(62142, fontFamily: "fontawesome");
  static const IconData user_o = IconData(62144, fontFamily: "fontawesome");
  static const IconData id_badge = IconData(62145, fontFamily: "fontawesome");
  static const IconData drivers_license =
      IconData(62146, fontFamily: "fontawesome");
  static const IconData id_card = IconData(62146, fontFamily: "fontawesome");
  static const IconData drivers_license_o =
      IconData(62147, fontFamily: "fontawesome");
  static const IconData id_card_o = IconData(62147, fontFamily: "fontawesome");
  static const IconData quora = IconData(62148, fontFamily: "fontawesome");
  static const IconData free_code_camp =
      IconData(62149, fontFamily: "fontawesome");
  static const IconData telegram = IconData(62150, fontFamily: "fontawesome");
  static const IconData thermometer_4 =
      IconData(62151, fontFamily: "fontawesome");
  static const IconData thermometer =
      IconData(62151, fontFamily: "fontawesome");
  static const IconData thermometer_full =
      IconData(62151, fontFamily: "fontawesome");
  static const IconData thermometer_3 =
      IconData(62152, fontFamily: "fontawesome");
  static const IconData thermometer_three_quarters =
      IconData(62152, fontFamily: "fontawesome");
  static const IconData thermometer_2 =
      IconData(62153, fontFamily: "fontawesome");
  static const IconData thermometer_half =
      IconData(62153, fontFamily: "fontawesome");
  static const IconData thermometer_1 =
      IconData(62154, fontFamily: "fontawesome");
  static const IconData thermometer_quarter =
      IconData(62154, fontFamily: "fontawesome");
  static const IconData thermometer_0 =
      IconData(62155, fontFamily: "fontawesome");
  static const IconData thermometer_empty =
      IconData(62155, fontFamily: "fontawesome");
  static const IconData shower = IconData(62156, fontFamily: "fontawesome");
  static const IconData bathtub = IconData(62157, fontFamily: "fontawesome");
  static const IconData s15 = IconData(62157, fontFamily: "fontawesome");
  static const IconData bath = IconData(62157, fontFamily: "fontawesome");
  static const IconData podcast = IconData(62158, fontFamily: "fontawesome");
  static const IconData window_maximize =
      IconData(62160, fontFamily: "fontawesome");
  static const IconData window_minimize =
      IconData(62161, fontFamily: "fontawesome");
  static const IconData window_restore =
      IconData(62162, fontFamily: "fontawesome");
  static const IconData times_rectangle =
      IconData(62163, fontFamily: "fontawesome");
  static const IconData window_close =
      IconData(62163, fontFamily: "fontawesome");
  static const IconData times_rectangle_o =
      IconData(62164, fontFamily: "fontawesome");
  static const IconData window_close_o =
      IconData(62164, fontFamily: "fontawesome");
  static const IconData bandcamp = IconData(62165, fontFamily: "fontawesome");
  static const IconData grav = IconData(62166, fontFamily: "fontawesome");
  static const IconData etsy = IconData(62167, fontFamily: "fontawesome");
  static const IconData imdb = IconData(62168, fontFamily: "fontawesome");
  static const IconData ravelry = IconData(62169, fontFamily: "fontawesome");
  static const IconData eercast = IconData(62170, fontFamily: "fontawesome");
  static const IconData microchip = IconData(62171, fontFamily: "fontawesome");
  static const IconData snowflake_o =
      IconData(62172, fontFamily: "fontawesome");
  static const IconData superpowers =
      IconData(62173, fontFamily: "fontawesome");
  static const IconData wpexplorer = IconData(62174, fontFamily: "fontawesome");
  static const IconData meetup = IconData(62176, fontFamily: "fontawesome");
}
