import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/redux/store.dart';

/// Delete a share link
///
/// Returns a promise with the status of the delete operation
Future<apiClient.DeleteShareLink> deleteShareLink(String linkId) async {
  return await apiClient.deleteShareLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a share is deleted.
///
/// Returns noting
Future<void> onShareDeleted(String linkId) async {
  await deleteShareLink(linkId);
}
