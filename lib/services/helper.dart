import 'package:psono/model/parsed_url.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;

/// Forms the full username including a domain if it is not yet part of the username.
String formFullUsername(String username, String domain) {
  if (username.contains('@')) {
    return username;
  } else {
    return username + '@' + domain;
  }
}

bool isIPv4Address(String address) {
  try {
    Uri.parseIPv4Address(address);
    return true;
  } on FormatException {
    return false;
  }
}

String dateTimeToIso(DateTime obj) {
  return obj.toUtc().toIso8601String();
}

DateTime isoToDateTime(String isoString) {
  return DateTime.parse(isoString);
}

/// parses an URL and returns an object with all details separated
ParsedUrl parseUrl(String url) {
  Uri parseResult = Uri.parse(url);
  String authority = parseResult.authority;
  if (authority.startsWith('www.')) {
    authority = authority.replaceFirst('www.', '');
  }
  List<String> splittedAuthority = authority.split(':');
  List<String> splittedDomain = splittedAuthority[0].split('.');
  String fullDomain = splittedAuthority[0];

  String topDomain;
  if (isIPv4Address(fullDomain)) {
    topDomain = fullDomain;
  } else if (splittedDomain.length >= 2) {
    topDomain = splittedDomain[splittedDomain.length - 2] +
        '.' +
        splittedDomain[splittedDomain.length - 1];
  }

  return ParsedUrl(
      scheme: parseResult.scheme,
      authority: authority,
      fullDomain: fullDomain,
      topDomain: topDomain,
      port: parseResult.port,
      path: parseResult.path,
      query: parseResult.query,
      fragment: parseResult.fragment);
}

/// parses an url and returns the domain part (removes any leading www. from the domain)
/// e.g. https://www.google.com/some/funny/path -> google.com
/// e.g. http://sub.example.com/some/funny/path -> sub.example.com
String getDomain(String url) {
  ParsedUrl parsedUrl;
  try {
    parsedUrl = parseUrl(url);
  } catch (e) {
    return '';
  }
  return parsedUrl.fullDomain;
}

/// Determines if a string contains a number.
bool hasNumber(String someString) {
  return RegExp(r'\d').hasMatch(someString);
}

/// Determines if a string contains an uppercase letter.
bool hasUppercaseLetter(String someString) {
  return RegExp(r'[A-Z]').hasMatch(someString);
}

/// Determines if a string contains a lowercase letter.
bool hasLowercaseLetter(String someString) {
  return RegExp(r'[a-z]').hasMatch(someString);
}

/// Determines if a string contains a special character.
bool hasSpecialCharacter(String someString) {
  return RegExp(r'''[ !@#$%^&*§()_+\-=\[\]{};':"\\|,.<>\/?]''')
      .hasMatch(someString);
}

/// Checks if array1 starts with array2
bool arrayStartsWith(List<String> array1, List<String> array2) {
  if (array1.length < array2.length) {
    return false;
  }

  for (var i = 0; i < array1.length; i++) {
    if (i == array2.length) {
      return true;
    }
    if (array1[i] != array2[i]) {
      return false;
    }
  }
  return true;
}

/// Determines if the password is a valid password.
/// If not, the function returns an error string.
String isValidPassword(
  String password,
  String password2,
  int minLength,
  int minComplexity,
) {
  if (password == null) {
    return "PASSWORD_TOO_SHORT";
  }
  if (password2 == null) {
    return "PASSWORDS_DONT_MATCH";
  }

  if (password.length < minLength) {
    return "PASSWORD_TOO_SHORT";
  }

  if (password != password2) {
    return "PASSWORDS_DONT_MATCH";
  }

  if (minComplexity > 0) {
    int complexity = 0;

    if (hasNumber(password)) {
      complexity = complexity + 1;
    }
    if (hasUppercaseLetter(password)) {
      complexity = complexity + 1;
    }
    if (hasLowercaseLetter(password)) {
      complexity = complexity + 1;
    }
    if (hasSpecialCharacter(password)) {
      complexity = complexity + 1;
    }

    if (complexity < minComplexity) {
      return "PASSWORD_NOT_COMPLEX_ENOUGH";
    }
  }

  return null;
}

///Splits a string into several chunks
List<String> splitStringInChunks(String str, int len) {
  int size = (str.length / len).ceil();
  List<String> chunks = new List(size);
  var offset = 0;

  for (var i = 0; i < size; ++i, offset += len) {
    chunks[i] = str.substring(offset, offset + len);
  }

  return chunks;
}

/// Checks that the username does not start with forbidden chars
String validateUsernameStart(String username, List<String> forbiddenChars) {
  for (var i = 0; i < forbiddenChars.length; i++) {
    if (username.substring(0, forbiddenChars[i].length) == forbiddenChars[i]) {
      return 'Usernames may not start with "' + forbiddenChars[i] + '"';
    }
  }
  return null;
}

/// Checks that the username does not end with forbidden chars
String validateUsernameEnd(String username, List<String> forbiddenChars) {
  for (var i = 0; i < forbiddenChars.length; i++) {
    if (username.substring(username.length - forbiddenChars[i].length) ==
        forbiddenChars[i]) {
      return 'Usernames may not end with "' + forbiddenChars[i] + '"';
    }
  }
  return null;
}

/// Checks that the username does not contain forbidden chars
String validateUsernameContain(String username, List<String> forbiddenChars) {
  for (var i = 0; i < forbiddenChars.length; i++) {
    if (username.indexOf(forbiddenChars[i]) != -1) {
      return 'Usernames may not contain "' + forbiddenChars[i] + '"';
    }
  }
  return null;
}

/// Determines if the password is a valid password.
/// If not, the function returns an error string.
String isValidUsername(String username) {
  var res = username.split("@");
  username = res[0];

  RegExp usernameRegexp = new RegExp(
    r"^[a-z0-9.\-]*$",
    caseSensitive: false,
    multiLine: false,
  );
  String error;
  if (!usernameRegexp.hasMatch(username)) {
    return 'USERNAME_VALIDATION_NAME_CONTAINS_INVALID_CHARS';
  }

  if (username.length < 2) {
    return 'USERNAME_VALIDATION_NAME_TOO_SHORT';
  }

  error = validateUsernameStart(username, [".", "-"]);
  if (error != null) {
    return error;
  }

  error = validateUsernameEnd(username, [".", "-"]);
  if (error != null) {
    return error;
  }

  error = validateUsernameContain(username, ["..", "--", '.-', '-.']);
  if (error != null) {
    return error;
  }

  return null;
}

/// Returns weather we have a valid email or not. We accept everything that follow x@x.
bool isValidEmail(String email) {
  var splitted = email.split('@');
  if (splitted.length != 2 ||
      splitted[0].length == 0 ||
      splitted[1].length == 0) {
    return false;
  }

  return true;
}

/// Returns a test function that can be used to filter according to the name and urlfilter
Function getPasswordFilter(test) {
  List<String> searchStrings = test.toLowerCase().split(" ");

  bool filter(datastoreEntry) {
    var containCounter = 0;
    for (var ii = searchStrings.length - 1; ii >= 0; ii--) {
      if (datastoreEntry.name == null) {
        continue;
      }

      if (datastoreEntry.name != null &&
          datastoreEntry.name.toLowerCase().indexOf(searchStrings[ii]) > -1) {
        containCounter++;
        continue;
      } else if (datastoreEntry.id != null &&
          datastoreEntry.id == searchStrings[ii]) {
        containCounter++;
        continue;
      } else if (datastoreEntry.shareId != null &&
          datastoreEntry.shareId == searchStrings[ii]) {
        containCounter++;
        continue;
      }

      if (datastoreEntry is datastoreModel.Item) {
        if (datastoreEntry.secretId != null &&
            datastoreEntry.secretId == searchStrings[ii]) {
          containCounter++;
          continue;
        } else if (datastoreEntry.urlfilter != null &&
            datastoreEntry.urlfilter.toLowerCase().indexOf(searchStrings[ii]) >
                -1) {
          containCounter++;
          continue;
        }
      }
    }
    return containCounter == searchStrings.length;
  }

  return filter;
}
