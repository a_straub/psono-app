import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/storage.dart';
import 'package:psono/model/config.dart';

class ScanConfigScreen extends StatefulWidget {
  static String tag = 'scan-config-screen';
  @override
  _ScanConfigScreenState createState() => _ScanConfigScreenState();
}

class _ScanConfigScreenState extends State<ScanConfigScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool redirect = false;
  QRViewController qrController;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (redirect) {
        Navigator.pushReplacementNamed(context, '/');
      }
    });

    return Scaffold(
      backgroundColor: Color(0xFF151f2b),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      child: new Text(FlutterI18n.translate(context, "ABORT")),
                      textColor: Color(0xFFb1b6c1),
                      onPressed: () {
                        qrController.pauseCamera();
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController qrController) {
    this.qrController = qrController;
    qrController.scannedDataStream.listen((String configString) async {
      qrController.pauseCamera();

      Map configMap;
      Config config;
      try {
        configMap = jsonDecode(configString);
        config = Config.fromJson(configMap);
      } on FormatException catch (e) {
        qrController.resumeCamera();
        return;
      }

      await storage.write(
        key: 'config',
        value: configString,
      );

      reduxStore.dispatch(
        new ConfigUpdatedAction(
          config,
        ),
      );
      setState(() {
        redirect = true;
      });
    });
  }

  @override
  void dispose() {
    qrController.dispose();
    super.dispose();
  }
}
