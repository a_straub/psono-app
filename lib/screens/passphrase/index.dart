import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:local_auth/local_auth.dart';
import 'package:flutter/services.dart';

import 'package:psono/redux/store.dart';
import 'package:psono/components/_index.dart' as components;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class PassphraseScreen extends StatefulWidget {
  PassphraseScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PassphraseScreenState createState() => new _PassphraseScreenState();
}

class _PassphraseScreenState extends State<PassphraseScreen> {
  bool isFingerprint;

  Future<Null> biometrics() async {
    final LocalAuthentication auth = new LocalAuthentication();
    bool authenticated = false;

    try {
      authenticated = await auth.authenticateWithBiometrics(
          localizedReason: 'Scan your fingerprint to authenticate',
          useErrorDialogs: true,
          stickyAuth: false);
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    if (authenticated) {
      setState(() {
        isFingerprint = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    bool enabled = reduxStore.state.lockscreenEnabled;
    String passphrase = reduxStore.state.lockscreenPassphrase;

    if (!enabled || passphrase.length == 0) {
      Navigator.pushReplacementNamed(context, '/datastore/');
    }

    List<int> myPass = [];
    List<String> passphraseStringList = passphrase.split('');
    for (int i = 0; i < passphraseStringList.length; i++) {
      myPass.add(int.parse(passphraseStringList[i]));
    }

    if (isFingerprint == null) {
      isFingerprint = false;
    }
    return components.LockView(
        title: FlutterI18n.translate(context, "PASSPHRASE"),
        passLength: myPass.length,
        fingerPrintImage: "assets/images/fingerprint.png",
        showFingerPass: true,
        fingerFunction: biometrics,
        signOut: () async {
          managerDatastoreUser.logout();
          Navigator.pushReplacementNamed(context, '/signin/');
        },
        fingerVerify: isFingerprint,
        borderColor: Colors.white,
        showWrongPassDialog: true,
        wrongPassContent:
            FlutterI18n.translate(context, "PASSPHRASE_INCORRECT"),
        wrongPassTitle: FlutterI18n.translate(context, "ERROR"),
        wrongPassCancelButtonText: FlutterI18n.translate(context, "CANCEL"),
        passCodeVerify: (passcode) async {
          for (int i = 0; i < myPass.length; i++) {
            if (passcode[i] != myPass[i]) {
              return false;
            }
          }

          return true;
        },
        onSuccess: () {
          Navigator.pushReplacementNamed(context, '/datastore/');
        });
  }
}
