import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';

part 'login_result_decrypted.g.dart';

@JsonSerializable()
class User {
  User({
    this.username,
    this.publicKey,
    this.privateKey,
    this.privateKeyNonce,
    this.userSauce,
  });

  final String username;
  @JsonKey(name: 'public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List publicKey;
  @JsonKey(name: 'private_key', fromJson: fromHex, toJson: toHex)
  final Uint8List privateKey;
  @JsonKey(name: 'private_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List privateKeyNonce;
  @JsonKey(name: 'user_sauce')
  final String userSauce;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class LoginResultDecrypted {
  LoginResultDecrypted({
    this.token,
    this.requiredMultifactors,
    this.sessionPublicKey,
    this.sessionSecretKey,
    this.sessionSecretKeyNonce,
    this.password,
    this.userValidator,
    this.userValidatorNonce,
    this.user,
  });

  final String token;
  @JsonKey(name: 'required_multifactors')
  final List<String> requiredMultifactors;
  @JsonKey(name: 'session_public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List sessionPublicKey;
  @JsonKey(name: 'session_secret_key', fromJson: fromHex, toJson: toHex)
  final Uint8List sessionSecretKey;
  @JsonKey(name: 'session_secret_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List sessionSecretKeyNonce;
  final String password;
  @JsonKey(name: 'user_validator', fromJson: fromHex, toJson: toHex)
  final Uint8List userValidator;
  @JsonKey(name: 'user_validator_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List userValidatorNonce;
  final User user;

  factory LoginResultDecrypted.fromJson(Map<String, dynamic> json) =>
      _$LoginResultDecryptedFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResultDecryptedToJson(this);
}
