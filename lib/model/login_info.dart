import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/helper.dart';

part 'login_info.g.dart';

@JsonSerializable()
class LoginInfo {
  LoginInfo({
    this.samlTokenId,
    this.username,
    this.authkey,
    this.deviceTime,
    this.deviceFingerprint,
    this.deviceDescription,
    this.password,
  });

  @JsonKey(name: 'saml_token_id')
  final String samlTokenId;
  final String username;
  final String authkey;
  @JsonKey(name: 'device_time', fromJson: isoToDateTime, toJson: dateTimeToIso)
  DateTime deviceTime;
  @JsonKey(name: 'device_fingerprint')
  final String deviceFingerprint;
  @JsonKey(name: 'device_description')
  final String deviceDescription;
  final String password;

  factory LoginInfo.fromJson(Map<String, dynamic> json) =>
      _$LoginInfoFromJson(json);

  Map<String, dynamic> toJson() => _$LoginInfoToJson(this);
}
