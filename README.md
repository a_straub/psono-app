# PSONO App - Password Manager

Master:  [![build status](https://gitlab.com/psono/psono-app/badges/master/pipeline.svg)](https://gitlab.com/psono/psono-app/commits/master) [![coverage report](https://gitlab.com/psono/psono-app/badges/master/coverage.svg)](https://gitlab.com/psono/psono-app/commits/master)

Develop: [![build status](https://gitlab.com/psono/psono-app/badges/develop/pipeline.svg)](https://gitlab.com/psono/psono-app/commits/develop) [![coverage report](https://gitlab.com/psono/psono-app/badges/develop/coverage.svg)](https://gitlab.com/psono/psono-app/commits/develop)

# Canonical source

The canonical source of the PSONO App is [hosted on GitLab.com](https://gitlab.com/psono/psono-app).

# Documentation

The documentation for the Psono app can be found here:

[Psono Documentation](https://doc.psono.com/)


# Some hints:

to generate models:

    flutter pub run build_runner build

or permanent

    flutter pub run build_runner watch

